package com.sourceit.listproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        myAdapter = new MyAdapter(this, generateUsers());
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(myAdapter);
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position) {
        Toast.makeText(MainActivity.this, myAdapter.getItem(position).getName(), Toast.LENGTH_SHORT).show();
    }

    public List<User> generateUsers() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(new User("Vasya" + i,
                    "+38 (050) 123 456" + (i % 10),
                    R.mipmap.ic_launcher));
        }
        return list;
    }

}
